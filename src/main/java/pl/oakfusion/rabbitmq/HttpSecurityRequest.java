package pl.oakfusion.rabbitmq;

import pl.oakfusion.security.HttpRequest;

import java.io.IOException;
import java.net.URI;
import java.net.http.*;

public class HttpSecurityRequest implements HttpRequest {

    @Override
    public String sendRequest(String uri, String headerName, String headerValue) throws IOException, InterruptedException {
        java.net.http.HttpRequest requestBlacklistJWT = java.net.http.HttpRequest
                .newBuilder()
                .uri(URI.create(uri))
                .header(headerName, headerValue)
                .build();

        HttpClient httpClient = HttpClient.newHttpClient();

        HttpResponse<String> responseBlacklistJWT =
                httpClient.send(requestBlacklistJWT, HttpResponse.BodyHandlers.ofString());
        return responseBlacklistJWT.body();
    }
}
