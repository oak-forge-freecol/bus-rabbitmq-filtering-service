package pl.oakfusion.rabbitmq;

import org.apache.http.impl.client.*;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.*;
import org.springframework.cloud.config.client.*;
import org.springframework.context.annotation.*;
import org.springframework.core.io.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.io.File;

@Configuration
public class SslConfig {

    @Value("${key-store-password:null}")
    String propertiesPassword;
    @Autowired
    ConfigClientProperties properties;

    @Primary
    @Bean
    public ConfigServicePropertySourceLocator configServicePropertySourceLocator() throws Exception {
        Resource resource = new ClassPathResource("core.p12");
        final File keyStoreFile = resource.getFile();

        final char[] password = (propertiesPassword == null) ? System.getenv("SERVICE_BOX_KEYSTORE_PASS").toCharArray() : propertiesPassword.toCharArray();
        SSLContext sslContext = SSLContexts.custom()
                .loadKeyMaterial(keyStoreFile, password, password)
                .loadTrustMaterial(keyStoreFile, password).build();
        CloseableHttpClient httpClient = HttpClients.custom().setSSLContext(sslContext).build();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
        ConfigServicePropertySourceLocator configServicePropertySourceLocator = new ConfigServicePropertySourceLocator(properties);
        configServicePropertySourceLocator.setRestTemplate(new RestTemplate(requestFactory));
        return configServicePropertySourceLocator;
    }
}
