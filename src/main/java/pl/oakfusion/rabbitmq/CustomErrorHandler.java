package pl.oakfusion.rabbitmq;

import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.util.ErrorHandler;

public class CustomErrorHandler implements ErrorHandler {

    @Override
    public void handleError(Throwable t) {
        throw new AmqpRejectAndDontRequeueException("Problem occurred. Dead Letter Queue will try to fix it, otherwise Exception will be returned");
    }
}
