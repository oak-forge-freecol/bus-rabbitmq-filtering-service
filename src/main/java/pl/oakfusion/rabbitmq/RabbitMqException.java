package pl.oakfusion.rabbitmq;

public class RabbitMqException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public RabbitMqException(Enum<ErrorCode> errorCode) {
        super(errorCode.name());
    }
}
