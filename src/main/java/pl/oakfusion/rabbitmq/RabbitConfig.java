package pl.oakfusion.rabbitmq;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.util.ErrorHandler;
import pl.oakfusion.rabbitmq.test.MessageTest;

import java.util.Objects;

@Configuration
@EnableConfigurationProperties({RabbitProperties.class})
public class RabbitConfig {
    @Autowired
    private RabbitProperties rabbitProperties;
    @Value("${authenticate.uri}")
    private String authenticateURI;

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper().registerModule(new JavaTimeModule());
    }

    @Bean
    @ConditionalOnMissingBean(ConnectionFactory.class)
    public ConnectionFactory cachingConnectionFactory() {
        RabbitConnectionFactoryBean connectionFactoryBean = new RabbitConnectionFactoryBean();
        RabbitProperties.Ssl ssl = rabbitProperties.getSsl();

        if (rabbitProperties.getHost() != null) {
            connectionFactoryBean.setHost(rabbitProperties.getHost());
            connectionFactoryBean.setPort(rabbitProperties.getPort());
        }
        if (rabbitProperties.getUsername() != null) {
            connectionFactoryBean.setUsername(rabbitProperties.getUsername());
        }
        if (rabbitProperties.getPassword() != null) {
            connectionFactoryBean.setPassword(rabbitProperties.getPassword());
        }
        if (ssl.isEnabled()) {
            connectionFactoryBean.setUseSSL(ssl.isEnabled());
            connectionFactoryBean.setKeyStore(ssl.getKeyStore());
            connectionFactoryBean.setKeyStorePassphrase(ssl.getKeyStorePassword());
            connectionFactoryBean.setTrustStore(ssl.getTrustStore());
            connectionFactoryBean.setTrustStorePassphrase(ssl.getTrustStorePassword());
        }
        connectionFactoryBean.afterPropertiesSet();
        try {
            return new CachingConnectionFactory(Objects.requireNonNull(connectionFactoryBean.getObject()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Bean("handleQueue")
    public Queue handleQueue() {
        return QueueBuilder
                .durable(rabbitProperties.getHandleQueueName())
                .withArgument("x-dead-letter-exchange", rabbitProperties.getDeadLetterExchangeName())
                .withArgument("x-dead-letter-routing-key", rabbitProperties.getDeadLetterRoutingKey())
                .build();
    }

    @Bean("handleExchange")
    public DirectExchange handleExchange() {
        return new DirectExchange(rabbitProperties.getHandleExchangeName());
    }

    @Bean
    public Binding handleBinding(@Qualifier("handleQueue") Queue queue,
                                 @Qualifier("handleExchange") DirectExchange directExchange) {
        return BindingBuilder
                .bind(queue)
                .to(directExchange)
                .with(rabbitProperties.getHandleRoutingKey());
    }

    @Bean("deadLetterQueue")
    public Queue deadLetterQueue() {
        return QueueBuilder
                .durable(rabbitProperties.getDeadLetterQueueName())
                .build();
    }

    @Bean("deadLetterExchange")
    public DirectExchange deadLetterExchange() {
        return new DirectExchange(rabbitProperties.getDeadLetterExchangeName());
    }

    @Bean
    public Binding deadLetterBinding(@Qualifier("deadLetterQueue") Queue queue,
                                     @Qualifier("deadLetterExchange") DirectExchange directExchange) {
        return BindingBuilder
                .bind(queue)
                .to(directExchange)
                .with(rabbitProperties.getDeadLetterRoutingKey());
    }

    @Bean("errorQueue")
    public Queue errorQueue() {
        return QueueBuilder
                .durable(rabbitProperties.getErrorQueueName())
                .build();
    }

    @Bean("errorExchange")
    public DirectExchange errorExchange() {
        return new DirectExchange(rabbitProperties.getErrorExchangeName());
    }

    @Bean
    public Binding errorBinding(@Qualifier("errorQueue") Queue queue,
                                @Qualifier("errorExchange") DirectExchange directExchange) {
        return BindingBuilder
                .bind(queue)
                .to(directExchange)
                .with(rabbitProperties.getErrorRoutingKey());
    }

    @Bean
    public Jackson2JsonMessageConverter converter(ObjectMapper objectMapper) {
        return new Jackson2JsonMessageConverter(objectMapper);
    }

    @Bean
    public HttpSecurityRequest httpSecurityRequest() {
        return new HttpSecurityRequest();
    }

    @Bean
    public RabbitMqConsumer rabbitMqConsumer(RabbitTemplate template,
                                             RabbitProperties rabbitProperties,
                                             @Qualifier("handleExchange") DirectExchange directExchange,
                                             Jackson2JsonMessageConverter converter,
                                             HttpSecurityRequest request) {
        RabbitMqConsumer consumer = new RabbitMqConsumer(template, rabbitProperties, directExchange, converter, request, authenticateURI);
        consumer.registerMessageHandler(MessageTest.class, message -> template.convertAndSend("discoExchange", "discoRoutingKey", message));
        return consumer;
    }

    @Bean
    public MessageListenerAdapter listenerAdapter(RabbitMqConsumer listener,
                                                  Jackson2JsonMessageConverter converter) {
        MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter();
        messageListenerAdapter.setDelegate(listener);
        messageListenerAdapter.setMessageConverter(converter);
        messageListenerAdapter.setDefaultListenerMethod("handleMessageWithSecurity");
        return messageListenerAdapter;
    }

    @Bean
    public ErrorHandler customErrorHandler() {
        return new CustomErrorHandler();
    }

    @Bean
    public SimpleMessageListenerContainer containerFactory(ConnectionFactory connectionFactory,
                                                           MessageListenerAdapter listenerAdapter,
                                                           ErrorHandler errorHandler) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(rabbitProperties.getHandleQueueName());
        container.setMessageListener(listenerAdapter);
        container.setErrorHandler(errorHandler);
        return container;
    }

}
