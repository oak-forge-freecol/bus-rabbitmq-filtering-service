package pl.oakfusion.rabbitmq.test;

import com.fasterxml.jackson.annotation.JsonCreator;
import pl.oakfusion.data.message.Message;

import java.time.LocalDateTime;

public class MessageTest extends Message {
    private String name;
    private String surname;

    @JsonCreator
    public MessageTest() {
        super(LocalDateTime.now());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "MessageTest{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
