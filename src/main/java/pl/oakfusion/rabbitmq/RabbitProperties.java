package pl.oakfusion.rabbitmq;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "rabbitmq")
public class RabbitProperties {
    private final Ssl ssl = new Ssl();
    private String host;
    private int port;
    private String username;
    private String password;
    private String handleQueueName;
    private String handleExchangeName;
    private String handleRoutingKey;
    private String deadLetterQueueName;
    private String deadLetterExchangeName;
    private String deadLetterRoutingKey;
    private String errorQueueName;
    private String errorExchangeName;
    private String errorRoutingKey;

    @Getter
    @Setter
    public static class Ssl {
        private boolean enabled;
        private String keyStore;
        private String keyStorePassword;
        private String trustStore;
        private String trustStorePassword;
    }

}
