package pl.oakfusion.rabbitmq;

public enum ErrorCode {
    TOKEN_BLACKLISTED_OR_EXPIRED,
    NO_TOKEN_PROVIDED,
    JSON_SYNTAX_EXCEPTION,
    JSON_MAPPING_EXCEPTION,
    UNEXPECTED_EXCEPTION

}
