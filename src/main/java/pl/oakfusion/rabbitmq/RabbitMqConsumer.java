package pl.oakfusion.rabbitmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.*;
import org.springframework.boot.json.JsonParseException;
import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.data.message.*;

import java.io.IOException;

@Slf4j
public class RabbitMqConsumer extends MessageBus {

    private final String rabbitMessageHeader = "HEADER_X_RETRIES_COUNT";
    private final RabbitTemplate rabbitTemplate;
    private final RabbitProperties rabbitProperties;
    private final DirectExchange exchange;
    private final Jackson2JsonMessageConverter converter;
    private final HttpSecurityRequest request;
    private final String authenticateURI;

    public RabbitMqConsumer(RabbitTemplate rabbitTemplate,
                            RabbitProperties rabbitProperties,
                            DirectExchange exchange,
                            Jackson2JsonMessageConverter converter,
                            HttpSecurityRequest request,
                            String authenticateURI) {
        this.rabbitTemplate = rabbitTemplate;
        this.rabbitProperties = rabbitProperties;
        this.exchange = exchange;
        this.converter = converter;
        this.request = request;
        this.authenticateURI = authenticateURI;
    }

    @Override
    public <M extends Message> void post(M message) {
        Object object =
                rabbitTemplate.convertSendAndReceive(exchange.getName(), rabbitProperties.getHandleRoutingKey(), message);
        ResponseEntity responseEntity = (ResponseEntity) object;
        assert responseEntity != null;
        if (401 == responseEntity.getHttpStatus()) {
            throw new RabbitMqException(ErrorCode.TOKEN_BLACKLISTED_OR_EXPIRED);
        }
    }

    public <M extends Message> ResponseEntity handleMessageWithSecurity(M message) {
        if (message.getAuthToken() == null) {
            throw new RabbitMqException(ErrorCode.NO_TOKEN_PROVIDED);
        }
        try {
            String idmResponse = request.sendRequest(authenticateURI, "token", message.getAuthToken());
            boolean isAuthenticated = Boolean.parseBoolean(idmResponse);
            if (!isAuthenticated) {
                throw new RabbitMqException(ErrorCode.TOKEN_BLACKLISTED_OR_EXPIRED);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        int i = handle(message);
        return i > 0 ? new ResponseEntity(200, "OK") : new ResponseEntity(422, "No message handlers found");
    }

    @RabbitListener(queues = "deadLetterQueue")
    public void deadLetterQueue(org.springframework.amqp.core.Message rabbitMessage) {
        final int MAX_RETRIES = 1;
        Integer retriesCount = (Integer) rabbitMessage
                .getMessageProperties()
                .getHeaders()
                .get(rabbitMessageHeader);

        if (rabbitMessage.getMessageProperties().getReplyTo() == null) {
            log.error("replyTo field not found, discarding message");
            return;
        }
        if (retriesCount == null) {
            retriesCount = 0;
        }
        if (retriesCount == MAX_RETRIES) {
            rabbitTemplate.send(
                    rabbitProperties.getErrorExchangeName(),
                    rabbitProperties.getErrorRoutingKey(),
                    rabbitMessage);
            return;
        }
        rabbitMessage
                .getMessageProperties()
                .getHeaders()
                .put(rabbitMessageHeader, ++retriesCount);
        rabbitTemplate.send(
                rabbitProperties.getHandleExchangeName(),
                rabbitProperties.getHandleRoutingKey(),
                rabbitMessage);
    }

    @RabbitListener(queues = "errorQueue")
    public ResponseEntity errorQueue(org.springframework.amqp.core.Message rabbitMessage) {
        try {
            Message message = (Message) converter.fromMessage(rabbitMessage, Message.class);

            if (message.getAuthToken() == null) {
                return new ResponseEntity(400, ErrorCode.NO_TOKEN_PROVIDED.name());
            } else {
                return new ResponseEntity(401, ErrorCode.TOKEN_BLACKLISTED_OR_EXPIRED.name());
            }
        } catch (JsonParseException e) {
            log.error(e.getMessage());
            return new ResponseEntity(400, ErrorCode.JSON_SYNTAX_EXCEPTION.name());
        } catch (MessageConversionException e) {
            log.error(e.getMessage());
            return new ResponseEntity(400, ErrorCode.JSON_MAPPING_EXCEPTION.name());
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity(400, ErrorCode.UNEXPECTED_EXCEPTION.name());
        }
    }

}
