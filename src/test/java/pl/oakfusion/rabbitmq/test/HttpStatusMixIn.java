package pl.oakfusion.rabbitmq.test;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.springframework.http.HttpStatus;

class HttpStatusMixIn {

    @JsonCreator
    public static HttpStatus resolve(int statusCode) {
        return HttpStatus.NO_CONTENT;
    }
}
