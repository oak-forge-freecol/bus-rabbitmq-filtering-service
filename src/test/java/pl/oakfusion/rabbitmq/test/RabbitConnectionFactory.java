package pl.oakfusion.rabbitmq.test;

import org.springframework.amqp.rabbit.connection.RabbitConnectionFactoryBean;

public class RabbitConnectionFactory {

    public final String host;
    public final int port;
    public final String username;
    public final String password;
    public final boolean useSSL;
    public final String keyStore;
    public final String keyStoreAndTrustStorePassword;
    public final String trustStore;

    public RabbitConnectionFactory(String host,
                                   int port,
                                   String username,
                                   String password,
                                   boolean useSSL,
                                   String keyStore,
                                   String keyStoreAndTrustStorePassword,
                                   String trustStore) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.useSSL = useSSL;
        this.keyStore = keyStore;
        this.keyStoreAndTrustStorePassword = keyStoreAndTrustStorePassword;
        this.trustStore = trustStore;
    }

    public RabbitConnectionFactoryBean setUp() {
        RabbitConnectionFactoryBean connectionFactoryBean = new RabbitConnectionFactoryBean();
        connectionFactoryBean.setHost(host);
        connectionFactoryBean.setPort(port);
        connectionFactoryBean.setUsername(username);
        connectionFactoryBean.setPassword(password);
        if (useSSL) {
            connectionFactoryBean.setUseSSL(useSSL);
            connectionFactoryBean.setKeyStore(keyStore);
            connectionFactoryBean.setKeyStorePassphrase(keyStoreAndTrustStorePassword);
            connectionFactoryBean.setTrustStore(trustStore);
            connectionFactoryBean.setTrustStorePassphrase(keyStoreAndTrustStorePassword);
        }
        connectionFactoryBean.afterPropertiesSet();
        return connectionFactoryBean;
    }
}

