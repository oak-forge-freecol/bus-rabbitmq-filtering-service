package pl.oakfusion.rabbitmq.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import pl.oakfusion.rabbitmq.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitBusTest {

    private final String properToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0aGVpbkBvYWtmdXNpb24ucGwiLCJleHAiOjE2MDEzMjkxNjUsInJvbCI6WyJVU0VSIl19.FJG-d7uZvaeMTJoGAMQerxyoB0ZrkMErJ6Dkbq7QZ2pzegfR-5zITQeVjafa39yyMbL7jHWo7o1f-ap3aP8HYA";
    private final String wrongToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0aGVpbkBvYWtmdXNpb24ucGwiLCJleHAiOjE2MDA0NjExNzUsInJvbCI6WyJVU0VSIl19.G8Yr5pzQkjNBsOCDv1XL7MS5scNHxc2p2PIjf426LawcvSL7yL2VfXJ2S-v2t9GtGvAHQMySewIvTykprP1oFw";
    @Autowired
    RabbitMqConsumer consumer;
    @Autowired
    private RabbitTemplate template;

    @Before
    public void setUp() {
        ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        objectMapper.addMixIn(ResponseEntity.class, ResponseEntityMixIn.class);
        objectMapper.addMixIn(HttpStatus.class, HttpStatusMixIn.class);
        Jackson2JsonMessageConverter converter = new Jackson2JsonMessageConverter(objectMapper);
        template.setMessageConverter(converter);
    }

    @Test
    public void Should_ReceiveNoException_When_TokenIsCorrect() {
        //given..
        MessageTest message = setUpMessage(properToken);

        //when..
        consumer.post(message);
    }

    @Test(expected = RabbitMqException.class)
    public void Should_ReceiveException_When_TokenIsIncorrect() {
        //given..
        MessageTest message = setUpMessage(wrongToken);

        //when..
        consumer.post(message);
    }

    private MessageTest setUpMessage(String token) {
        MessageTest message = new MessageTest();
        message.setName("Kasia");
        message.setSurname("Kowalska");
        message.setAuthToken(token);
        return message;
    }
}
