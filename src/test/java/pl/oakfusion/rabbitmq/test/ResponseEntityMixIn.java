package pl.oakfusion.rabbitmq.test;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.http.HttpStatus;
import org.springframework.util.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseEntityMixIn {

    private final Object body;
    private final MultiValueMap<String, String> headers;
    private final HttpStatus status;

    @JsonCreator
    public ResponseEntityMixIn(@JsonProperty("body") Object body,
                               @JsonDeserialize(as = LinkedMultiValueMap.class) @JsonProperty("headers") MultiValueMap<String, String> headers,
                               @JsonProperty("statusCodeValue") HttpStatus status) {
        this.body = body;
        this.headers = headers;
        this.status = status;
    }

    public Object getBody() {
        return body;
    }

    public MultiValueMap<String, String> getHeaders() {
        return headers;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
