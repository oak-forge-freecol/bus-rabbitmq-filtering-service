package pl.oakfusion.rabbitmq.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.amqp.*;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.connection.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.data.message.Message;

import java.util.Objects;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitBusConnectionTest extends MessageBus {

    private final String host = "localhost";
    private final int port = 5671;
    private final String username = "admin";
    private final String password = "admin";
    private final boolean useSSL = true;
    private final String keyStore = "client_key.p12";
    private final String keyStoreAndTrustStorePassword = "bunnies";
    private final String trustStore = "server_store.jks";
    private final String routingKey = "handleRoutingKey";
    private MessageTest message;
    private RabbitTemplate template;
    @Autowired
    private @Qualifier("handleExchange")
    DirectExchange exchange;

    @Before
    public void setUpMessage() {
        message = new MessageTest();
        message.setName("Kasia");
        message.setSurname("Kowalska");
    }

    @Override
    public <M extends Message> void post(M message) {
        template.convertAndSend(exchange.getName(), routingKey, message);
    }

    @Test(expected = AmqpIOException.class)
    public void Should_ThrowException_When_NoSslIsProvided() {
        //given..
        template = rabbitTemplateSetup(false, password);

        //when..
        post(message);
    }

    @Test(expected = AmqpAuthenticationException.class)
    public void Should_ThrowException_When_WrongPasswordIsProvided() {
        //given..
        template = rabbitTemplateSetup(useSSL, "WrongPassword");

        //when..
        post(message);
    }

    private RabbitTemplate rabbitTemplateSetup(boolean ssl, String providedPassword) {
        RabbitConnectionFactoryBean connectionFactoryBean =
                new RabbitConnectionFactory(host, port, username, providedPassword, ssl, keyStore, keyStoreAndTrustStorePassword, trustStore).setUp();

        RabbitTemplate template = null;
        try {
            template = new RabbitTemplate(
                    new CachingConnectionFactory(
                            Objects.requireNonNull(connectionFactoryBean.getObject())
                    )
            );
            template.setMessageConverter(
                    new Jackson2JsonMessageConverter(
                            new ObjectMapper().registerModule(new JavaTimeModule())
                    )
            );
            return template;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return template;
    }

}
